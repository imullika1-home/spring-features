package com.example.springfeatures.configuration

import io.r2dbc.postgresql.PostgresqlConnectionConfiguration
import io.r2dbc.postgresql.PostgresqlConnectionFactory
import io.r2dbc.spi.ConnectionFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration
import org.springframework.data.r2dbc.config.AbstractR2dbcConfiguration
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories
import org.springframework.transaction.annotation.EnableTransactionManagement


/**
 * PostgreSQL reactive client configuration
 * @property username [String] user name for database connection
 * @property password [String] password for database connection
 * @property connectionURL [String] connection url to database
 */
@Configuration
@EnableR2dbcRepositories
@EnableTransactionManagement
class ConfigurationReactivePostgreSQL : AbstractR2dbcConfiguration() {
    @Value("\${spring.r2dbc.username}")
    private lateinit var username: String

    @Value("\${spring.r2dbc.password}")
    private lateinit var password: String

    @Value("\${spring.r2dbc.url}")
    private lateinit var connectionURL: String

    /**
     * Configure reactive postgresql database client
     */
    override fun connectionFactory(): ConnectionFactory = PostgresqlConnectionFactory(
        PostgresqlConnectionConfiguration
            .builder()
            .host(connectionURL)
            .username(username)
            .password(password)
            .build()
    )
}