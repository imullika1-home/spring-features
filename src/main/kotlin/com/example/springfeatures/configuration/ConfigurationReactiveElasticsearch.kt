package com.example.springfeatures.configuration

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration
import org.springframework.data.elasticsearch.client.ClientConfiguration
import org.springframework.data.elasticsearch.client.reactive.ReactiveElasticsearchClient
import org.springframework.data.elasticsearch.client.reactive.ReactiveRestClients
import org.springframework.data.elasticsearch.config.AbstractReactiveElasticsearchConfiguration
import org.springframework.data.elasticsearch.repository.config.EnableReactiveElasticsearchRepositories

/**
 * Elasticsearch reactive client configuration
 * @property username [String] user name for database connection
 * @property password [String] password for database connection
 * @property connectionURL [String] connection url to database
 */
@Configuration
@EnableReactiveElasticsearchRepositories
class ConfigurationReactiveElasticsearch : AbstractReactiveElasticsearchConfiguration() {
    @Value("\${spring.elasticsearch.username}")
    private lateinit var username: String

    @Value("\${spring.elasticsearch.password}")
    private lateinit var password: String

    @Value("\${spring.elasticsearch.uris}")
    private lateinit var connectionURL: String

    /**
     * Configure reactive elasticsearch database client
     */
    override fun reactiveElasticsearchClient(): ReactiveElasticsearchClient = ReactiveRestClients.create(
        ClientConfiguration
            .builder()
            .connectedTo(connectionURL)
            .withBasicAuth(username, password)
            .build()
    )
}
