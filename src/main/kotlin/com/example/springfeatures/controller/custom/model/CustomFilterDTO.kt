package com.example.springfeatures.controller.custom.model

/**
 * Data transfer object representing custom objects filtering request
 * @param ids [Iterable] of [String] ids of custom objects
 * @param streets [Iterable] of [String] streets of custom objects
 * @param devices [Iterable] of [String] devices of custom objects
 * @param offset [Int] offset count
 * @param limit [Int] results count limit
 * @param threshold [Double] threshold of matching request
 */
class CustomFilterDTO(
    val ids: Iterable<String>,
    val streets: Iterable<String>,
    val devices: Iterable<String>,
    val offset: Int = 0,
    val limit: Int = 100,
    val threshold: Double
)