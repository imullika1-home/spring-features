package com.example.springfeatures.controller.custom.model

import com.fasterxml.jackson.annotation.JsonProperty

/**
 * Data transfer object representing custom's object address
 * @param city [String] custom's address city
 * @param street [String] custom's address street
 */
class CustomAddressDTO(
    @JsonProperty("city")
    val city: String,
    @JsonProperty("street")
    val street: String
)