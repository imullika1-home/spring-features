package com.example.springfeatures.controller.custom.model

import com.example.springfeatures.service.custom.model.CustomAddressDataModels
import com.example.springfeatures.service.custom.model.CustomDataModels
import com.fasterxml.jackson.annotation.JsonProperty
import reactor.core.publisher.Mono

/**
 * Data transfer object representing custom object
 * @param id [String] custom's id
 * @param name [String] custom's name
 * @param address [CustomAddressDTO] custom's address
 * @param devices [MutableList] of [String]
 */
class CustomDTO(
    @JsonProperty("id")
    val id: String,
    @JsonProperty("name")
    val name: String,
    @JsonProperty("address")
    val address: CustomAddressDTO,
    @JsonProperty("devices")
    val devices: MutableList<String>
) {
    /**
     * Convert data transfer object to data model
     * @return [Mono] of [CustomDataModels]
     */
    fun toDM(): Mono<CustomDataModels> = Mono
        .just(CustomDataModels(id, name, CustomAddressDataModels(address.city, address.street), devices))
}