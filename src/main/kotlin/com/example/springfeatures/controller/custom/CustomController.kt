package com.example.springfeatures.controller.custom

import com.example.springfeatures.controller.custom.model.CustomDTO
import com.example.springfeatures.controller.custom.model.CustomFilterDTO
import com.example.springfeatures.service.custom.CustomService
import com.example.springfeatures.service.custom.model.CustomDataModels
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

/**
 * Handler of /custom/ * endpoints
 * @param customService [CustomService] service for manipulating [CustomDataModels] custom objects
 */
@RestController
@RequestMapping("/custom")
class CustomController(private val customService: CustomService) {
    /**
     * POST handler of /custom/add endpoint - add or update custom object at elasticsearch database
     * @return [Mono] of [CustomDTO]
     */
    @PostMapping("/add")
    fun add(@RequestBody custom: CustomDTO): Mono<CustomDTO> = customService
        .add(custom)
        .flatMap { it.toDTO() }

    /**
     * POST handler of /custom/filter endpoint - filter custom objects by ids, streets, devices and limit them by count and matching percent
     * @param filter [CustomFilterDTO] filter model
     * @return [Flux] of [CustomDTO]
     */
    @PostMapping("/filter")
    fun filter(@RequestBody filter: CustomFilterDTO): Flux<CustomDTO> = customService
        .filter(filter.ids, filter.streets, filter.devices, filter.offset, filter.limit, filter.threshold)
        .flatMap { it.toDTO() }
}