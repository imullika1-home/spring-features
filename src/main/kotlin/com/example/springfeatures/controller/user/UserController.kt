package com.example.springfeatures.controller.user

import com.example.springfeatures.controller.user.model.UserDTO
import com.example.springfeatures.service.user.UserService
import com.example.springfeatures.service.user.model.UserDataModels
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.reactive.asFlow
import kotlinx.coroutines.reactor.awaitSingle
import kotlinx.coroutines.reactor.awaitSingleOrNull
import org.springframework.data.elasticsearch.core.query.ByQueryResponse
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Flux

/**
 * Handler of /users/ * endpoints
 * @property userService [UserService] service for manipulating [UserDataModels] user objects
 */
@RestController
@RequestMapping("/users")
class UserController(private val userService: UserService) {

    /************
     * FIND ALL *
     ************/

    /**
     * GET handler of /users/pg endpoint - find all users at PostgreSQL database
     * @return [Flux] of [UserDTO]
     */
    @GetMapping("/pg")
    fun findAllPg(): Flux<UserDTO> = userService.findAllPostgreSQL().flatMap { it.toDTO() }

    /**
     * GET handler of /users/es endpoint - find all users at elasticsearch database
     * @return [Flux] of [UserDTO]
     */
    @GetMapping("/es")
    fun findAllEs(): Flux<UserDTO> = userService.findAllElasticsearch().flatMap { it.content.toDTO() }

    /**
     * GET handler of /users/es/repo endpoint - find all users at elasticsearch database using repository
     * @return [Flux] of [UserDTO]
     */
    @GetMapping("/es/repo")
    fun findAllEsRepo(): Flux<UserDTO> = userService.findAllElasticsearchRepo().flatMap { it.toDTO() }

    /*****************
     * ADD OR UPDATE *
     *****************/

    /**
     * POST handler of /users/add-or-update endpoint - add or update user at postgresql and elasticsearch databases
     * @param user [UserDTO] user
     * @return [ResponseEntity] of [HttpStatus]
     */
    @PostMapping("/add-or-update")
    suspend fun add(@RequestBody user: UserDTO): ResponseEntity<Any> = userService
        .addOrUpdate(user.toDM().awaitSingle())
        ?.let { t ->
            if (t.all { it != null }) ResponseEntity(HttpStatus.OK)
            else ResponseEntity(HttpStatus.BAD_REQUEST)
        } ?: ResponseEntity(HttpStatus.BAD_REQUEST)

    /**
     * POST handler of /users/es/add-or-update endpoint - add or update user at elasticsearch database
     * @param user [UserDTO] user
     * @return [ResponseEntity] of [HttpStatus.OK]
     */
    @PostMapping("/es/add-or-update")
    suspend fun addOrUpdateEs(@RequestBody user: UserDTO): ResponseEntity<Any> = userService
        .addOrUpdateEs(user.toDM().awaitSingle())
        .run { ResponseEntity(HttpStatus.OK) }

    /**
     * POST handler of /users/es/repo/add-or-update endpoint - add or update user at elasticsearch database using repository
     * @param user [UserDTO] user
     * @return [ResponseEntity] of [HttpStatus.OK]
     */
    @PostMapping("/es/repo/add-or-update")
    suspend fun addOrUpdateEsRepo(@RequestBody user: UserDTO): ResponseEntity<Any> = userService
        .addOrUpdateEsRepo(user.toDM().awaitSingle())
        .run { ResponseEntity(HttpStatus.OK) }

    /**********************
     * BULK ADD OR UPDATE *
     **********************/

    /**
     * POST handler of /users/es/bulk/add-or-update endpoint - add or update users at elasticsearch database
     * @param users [Iterable] of [UserDTO] users
     * @return [Flow] of [UserDTO]
     */
    @PostMapping("/es/bulk/add-or-update")
    suspend fun addOrUpdateEsBulk(@RequestBody users: Iterable<UserDTO>): Flow<UserDTO> = userService
        .addOrUpdateEsBulk(users
            .asFlow()
            .map { it.toDM().awaitSingle() }
            .toList()
        )
        .asFlow()
        .map { it.toDTO().awaitSingle() }

    /**
     * POST handler of /users/es/repo/bulk/add-or-update endpoint - add or update users at elasticsearch database using repository
     * @param users [Iterable] of [UserDTO] users
     * @return [Flow] of [UserDTO]
     */
    @PostMapping("/es/repo/bulk/add-or-update")
    suspend fun addOrUpdateEsRepoBulk(@RequestBody users: Iterable<UserDTO>): Flow<UserDTO> = userService
        .addOrUpdateEsRepoBulk(users
            .asFlow()
            .map { it.toDM().awaitSingle() }
            .toList()
        )
        .asFlow()
        .map { it.toDTO().awaitSingle() }

    /**************
     * FIND BY ID *
     **************/

    /**
     * GET handler of /users/pg/{id} endpoint - find user by id at postgresql database
     * @param id [String] user's id
     * @return [UserDTO]
     */
    @GetMapping("/pg/{id}")
    suspend fun findByIdPg(@PathVariable("id") id: String): UserDTO? = userService
        .findByIdPostgreSQL(id)
        ?.toDTO()
        ?.awaitSingleOrNull()

    /**
     * GET handler of /users/es/{id} endpoint - find user by id at elasticsearch database
     * @param id [String] user's id
     * @return [UserDTO]
     */
    @GetMapping("/es/{id}")
    suspend fun findByIdEs(@PathVariable("id") id: String): UserDTO? = userService
        .findByIdElasticsearch(id)
        ?.toDTO()
        ?.awaitSingleOrNull()

    /**
     * GET handler of /users/es/repo/{id} endpoint - find user by id at elasticsearch database using repository
     * @param id [String] user's id
     * @return [UserDTO]
     */
    @GetMapping("/es/repo/{id}")
    suspend fun findByIdEsRepo(@PathVariable("id") id: String): UserDTO? = userService
        .findByIdElasticsearchRepo(id)
        ?.toDTO()
        ?.awaitSingleOrNull()

    /****************
     * DELETE BY ID *
     ****************/

    /**
     * GET handler of /users/del/{id} endpoint - delete user by id at postgresql and elasticsearch databases
     * @param id [String] user's id
     * @return [ResponseEntity] of [HttpStatus]
     */
    @GetMapping("/del/{id}")
    suspend fun deleteById(@PathVariable("id") id: String): ResponseEntity<Any> = userService
        .deleteById(id)?.let { t ->
            if (t.all { it != null } && t.t1.isNotEmpty() && t.t2 == 1) ResponseEntity(HttpStatus.OK)
            else ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        } ?: ResponseEntity(HttpStatus.BAD_REQUEST)

    /**
     * GET handler of /users/es/del/{id} endpoint - delete user by id at elasticsearch database
     * @param id [String] user's id
     * @return [ResponseEntity] of [HttpStatus.OK]
     */
    @GetMapping("/es/del/{id}")
    suspend fun deleteByIdEs(@PathVariable("id") id: String): ResponseEntity<Any> = userService
        .deleteByIdEs(id)
        .run { ResponseEntity(HttpStatus.OK) }

    /**
     * GET handler of /users/es/del/repo/{id} endpoint - delete user by id at elasticsearch database using repository
     * @param id [String] user's id
     * @return [ResponseEntity] of [HttpStatus.OK]
     */
    @GetMapping("/es/del/repo/{id}")
    suspend fun deleteByIdEsRepo(@PathVariable("id") id: String): ResponseEntity<Any> = userService
        .deleteByIdEsRepo(id)
        .run { ResponseEntity(HttpStatus.OK) }

    /*********************
     * BULK DELETE BY ID *
     *********************/

    /**
     * POST handler of /users/es/del/bulk endpoint - delete users by id at elasticsearch database
     * @param ids [Iterable] of [String] users ids
     */
    @PostMapping("/es/del/bulk")
    suspend fun deleteByIdEsBulk(@RequestBody ids: Iterable<String>): ByQueryResponse = userService
        .deleteByIdEsBulk(ids)

    /**
     * POST handler of /users/es/del/repo/bulk endpoint - delete users by id at elasticsearch database using repository
     * @param ids [Iterable] of [String] users ids
     */
    @PostMapping("/es/del/repo/bulk")
    suspend fun deleteByIdEsRepoBulk(@RequestBody ids: Iterable<String>): Void? = userService
        .deleteByIdElasticsearchRepoBulk(ids)

    /*********************
     * FIND BY NAME LIKE *
     *********************/

    /**
     * GET handler of /users/pg/like endpoint - find users by name like at postgresql database
     * @param name [String] user's name
     * @param limit [Int] limit of result
     * @return [Flux] of [UserDTO]
     */
    @GetMapping("/pg/like")
    fun findNameLikePg(@RequestParam("name") name: String, @RequestParam("limit") limit: Int): Flux<UserDTO> =
        userService.findNameLikePostgreSQL(name, limit).flatMap { it.toDTO() }

    /**
     * GET handler of /users/es/like endpoint - find users by name like at elasticsearch database
     * @param name [String] user's name
     * @param limit [Int] limit of result
     * @return [Flux] of [UserDTO]
     */
    @GetMapping("/es/like")
    fun findNameLikeEs(
        @RequestParam("name") name: String,
        @RequestParam("limit") limit: Int
    ): Flux<UserDTO> = userService.findNameLikeElasticsearch(name, limit).flatMap { it.toDTO() }

    /**
     * GET handler of /users/es/like/repo endpoint - find users by name like at elasticsearch database using repository
     * @param name [String] user's name
     * @param limit [Int] limit of result
     * @return [Flux] of [UserDTO]
     */
    @GetMapping("/es/like/repo")
    fun findNameLikeEsRepo(
        @RequestParam("name") name: String,
        @RequestParam("limit") limit: Int
    ): Flux<UserDTO> = userService.findNameLikeElasticsearchRepository(name, limit).flatMap { it.toDTO() }
}