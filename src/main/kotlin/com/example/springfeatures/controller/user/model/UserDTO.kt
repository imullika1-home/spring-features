package com.example.springfeatures.controller.user.model

import com.example.springfeatures.service.user.model.UserDataModels
import com.fasterxml.jackson.annotation.JsonProperty
import reactor.core.publisher.Mono

/**
 * Data transfer object representing user
 * @param id [String] user's id
 * @param name [String] user's name
 */
data class UserDTO(
    @JsonProperty("id")
    val id: String = "",
    @JsonProperty("name")
    val name: String = ""
) {
    /**
     * Convert data transfer object to data model
     * @return [Mono] of [UserDataModels]
     */
    fun toDM(): Mono<UserDataModels> = Mono.just(UserDataModels(id, name))
}