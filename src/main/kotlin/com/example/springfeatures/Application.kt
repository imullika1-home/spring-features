package com.example.springfeatures

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.web.reactive.config.EnableWebFlux

@EnableWebFlux
@SpringBootApplication
class SpringFeaturesApplication

fun main(args: Array<String>) {
    runApplication<SpringFeaturesApplication>(*args)
}
