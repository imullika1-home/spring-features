package com.example.springfeatures.service.custom

import com.example.springfeatures.controller.custom.model.CustomDTO
import com.example.springfeatures.service.custom.model.CustomDataModels
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

interface CustomService {
    /**
     * Add or update custom object at elasticsearch database
     * @param custom [CustomDTO] custom object
     * @return [Mono] of [CustomDataModels]
     */
    fun add(custom: CustomDTO): Mono<CustomDataModels>

    /**
     * Filter custom objects by ids, streets, devices and limit them by count and matching percent
     * @param ids [Iterable] of [String] ids of custom objects
     * @param streets [Iterable] of [String] streets of custom objects
     * @param devices [Iterable] of [String] devices of custom objects
     * @param offset [Int] offset count
     * @param limit [Int] result limit count
     * @param threshold [Double] threshold of matching request
     * @return [Flux] of [CustomDataModels]
     */
    fun filter(
        ids: Iterable<String>,
        streets: Iterable<String>,
        devices: Iterable<String>,
        offset: Int,
        limit: Int,
        threshold: Double
    ): Flux<CustomDataModels>

//    fun findAllByIdInidsOrAddress_StreetInstreetsOrDevicesIndevices(
//        ids: Flux<String>,
//        streets: Flux<String>,
//        devices: Flux<String>
//    ): Flux<CustomDataModels>
}