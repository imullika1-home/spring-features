package com.example.springfeatures.service.custom.model

import com.example.springfeatures.controller.custom.model.CustomAddressDTO
import com.example.springfeatures.controller.custom.model.CustomDTO
import org.springframework.data.annotation.Id
import org.springframework.data.elasticsearch.annotations.Document
import org.springframework.data.elasticsearch.annotations.Field
import org.springframework.data.elasticsearch.annotations.FieldType
import reactor.core.publisher.Mono

/**
 * Data model representing custom object
 * @param id [String] custom's id
 * @param name [String] custom's name
 * @param address [CustomAddressDTO] custom's address
 * @param devices [MutableList] of [String]
 */
@Document(indexName = "customs")
data class CustomDataModels(
    @Id
    @Field(name = "id")
    val id: String,
    @Field(name = "name")
    val name: String,
    @Field(name = "address", type = FieldType.Nested)
    val address: CustomAddressDataModels,
    @Field(name = "devices", type = FieldType.Nested)
    val devices: MutableList<String>
) {
    /**
     * Convert data model to data transfer object
     * @return [Mono] of [CustomDTO]
     */
    fun toDTO(): Mono<CustomDTO> = Mono
        .just(CustomDTO(id, name, CustomAddressDTO(address.city, address.street), devices))
}