package com.example.springfeatures.service.custom

import com.example.springfeatures.controller.custom.model.CustomDTO
import com.example.springfeatures.repository.custom.CustomRepository
import com.example.springfeatures.service.custom.model.CustomDataModels
import org.elasticsearch.index.query.QueryBuilders
import org.springframework.data.domain.PageRequest
import org.springframework.data.elasticsearch.core.ReactiveElasticsearchOperations
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import kotlin.math.floor
import kotlin.math.roundToInt

@Service
class CustomServiceImpl(
    private val customRepository: CustomRepository,
    private val elasticsearchClient: ReactiveElasticsearchOperations
) : CustomService {
    /**
     * Add or update custom object at elasticsearch database
     * @param custom [CustomDTO] custom object
     * @return [Mono] of [CustomDataModels]
     */
    override fun add(custom: CustomDTO) = elasticsearchClient.save(custom.toDM())

    /**
     * Filter custom objects by ids, streets, devices and limit them by count and matching percent
     * @param ids [Iterable] of [String] ids of custom objects
     * @param streets [Iterable] of [String] streets of custom objects
     * @param devices [Iterable] of [String] devices of custom objects
     * @param offset [Int] offset count
     * @param limit [Int] result limit count
     * @param threshold [Double] threshold of matching request
     * @return [Flux] of [CustomDataModels]
     */
    override fun filter(
        ids: Iterable<String>,
        streets: Iterable<String>,
        devices: Iterable<String>,
        offset: Int,
        limit: Int,
        threshold: Double
    ) = elasticsearchClient.search(
        NativeSearchQueryBuilder()
            .withQuery(
                QueryBuilders
                    .boolQuery()
                    .should(QueryBuilders.idsQuery().addIds(*ids.toList().toTypedArray()))
                    .should(QueryBuilders.termsQuery("address.street", *streets.toList().toTypedArray()))
                    .should(QueryBuilders.termsQuery("devices", *devices.toList().toTypedArray()))
                    .minimumShouldMatch("${(threshold * 100).roundToInt()}%")
            )
            .withPageable(PageRequest.of(floor(offset.toDouble() / limit).toInt(), limit))
            .withMaxResults(limit)
            .build(),
        CustomDataModels::class.java
    ).map { it.content }

//    override fun findAllByIdInidsOrAddress_StreetInstreetsOrDevicesIndevices(
//        ids: Flux<String>,
//        streets: Flux<String>,
//        devices: Flux<String>,
//    ) = customRepository.findAllByIdInOrAddress_StreetInOrDevicesIn(
//        ids, streets, devices
//    )
}