package com.example.springfeatures.service.custom.model

import org.springframework.data.elasticsearch.annotations.Field

/**
 * Data model representing custom's object address
 * @param city [String] custom's address city
 * @param street [String] custom's address street
 */
data class CustomAddressDataModels(
    @Field(name = "city")
    val city: String,
    @Field(name = "street")
    val street: String
)