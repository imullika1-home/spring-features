package com.example.springfeatures.service.user

import com.example.springfeatures.service.user.model.UserDataModels
import org.springframework.data.elasticsearch.core.SearchHit
import org.springframework.data.elasticsearch.core.query.ByQueryResponse
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.util.function.Tuple2

/**
 * getByFirstAndSecondName, bulkDelete
 */
interface UserService {
    /*****************
     * ADD OR UPDATE *
     *****************/

    /**
     * Add or update user at elasticsearch and postgresql databases
     * @param user [UserDataModels] user model
     * @return [reactor.util.function.Tuple2] of two [Mono]'s representing result of es & pg result
     */
    suspend fun addOrUpdate(user: UserDataModels): Tuple2<UserDataModels, UserDataModels>?

    /**
     * Add or update user at elasticsearch database
     * @param user [UserDataModels] user
     * @return [UserDataModels]
     */
    suspend fun addOrUpdateEs(user: UserDataModels): UserDataModels

    /**
     * Add or update user in elasticsearch database using repository
     * @param user [UserDataModels] user
     * @return [UserDataModels]
     */
    suspend fun addOrUpdateEsRepo(user: UserDataModels): UserDataModels

    /**********************
     * ADD OR UPDATE BULK *
     **********************/

    /**
     * Bulk add or update users at elasticsearch database
     * @param users [Iterable] of [UserDataModels]
     * @return [Flux] of [UserDataModels]
     */
    fun addOrUpdateEsBulk(users: Iterable<UserDataModels>): Flux<UserDataModels>

    /**
     * Bulk add or update users at elasticsearch database using repository
     * @param users [Iterable] of [UserDataModels]
     * @return [Flux] of [UserDataModels]
     */
    fun addOrUpdateEsRepoBulk(users: Iterable<UserDataModels>): Flux<UserDataModels>

    /************
     * FIND ALL *
     ************/

    /**
     * Find all users from postgresql data-base
     * @return [Flux] of [UserDataModels]
     */
    fun findAllPostgreSQL(): Flux<UserDataModels>

    /**
     * Find all users from elasticsearch database
     * @return [Flux] of [UserDataModels]
     */
    fun findAllElasticsearch(): Flux<SearchHit<UserDataModels>>

    /**
     * Find all users from elasticsearch database using repository
     * @return [Flux] of [UserDataModels]
     */
    fun findAllElasticsearchRepo(): Flux<UserDataModels>

    /**************
     * FIND BY ID *
     **************/

    /**
     * Find user by id in elasticsearch database
     * @param id [String] user's id
     * @return [UserDataModels] user
     */
    suspend fun findByIdElasticsearch(id: String): UserDataModels?

    /**
     * Find user by id in elasticsearch database using repository
     * @param id [String] user's id
     * @return [UserDataModels] user
     */
    suspend fun findByIdElasticsearchRepo(id: String): UserDataModels?

    /**
     * Find user by id in postgresql database
     * @param id [String] user's id
     * @return [UserDataModels] user
     */
    suspend fun findByIdPostgreSQL(id: String): UserDataModels?

    /****************
     * DELETE BY ID *
     ****************/

    /**
     * Delete user by id from es & pg data-bases
     * @param id [String] user's id
     * @return [reactor.util.function.Tuple2] of two [Mono]'s representing result of es & pg result
     */
    suspend fun deleteById(id: String): Tuple2<String, Int>?

    /**
     * Delete user by id from es database
     * @param id [String] user's id
     * @return [String] user's id
     */
    suspend fun deleteByIdEs(id: String): String?

    /**
     * Delete user by id from es database using repo
     * @param id [String] user's id
     * @return [Void]
     */
    suspend fun deleteByIdEsRepo(id: String): Void?

    suspend fun deleteByIdEsBulk(ids: Iterable<String>): ByQueryResponse

    suspend fun deleteByIdElasticsearchRepoBulk(ids: Iterable<String>): Void?

    /******************
     * FIND NAME LIKE *
     ******************/

    /**
     * Find users that name like in args and limit result count
     * @param name [String] user's name that will be like
     * @param limit [Int] limit of result count
     * @return [Flux] of [UserDataModels]
     */
    fun findNameLikePostgreSQL(name: String, limit: Int): Flux<UserDataModels>

    /**
     * Find users that name like and limit result count
     * @param name [String] user's name
     * @param limit [Int] limit
     * @return [Flux] of [UserDataModels]
     */
    fun findNameLikeElasticsearch(name: String, limit: Int): Flux<UserDataModels>

    /**
     * Find users that name like and limit result count using repository
     * @param name [String] user's name
     * @param limit [Int] limit
     * @return [Flux] of [UserDataModels]
     */
    fun findNameLikeElasticsearchRepository(name: String, limit: Int): Flux<UserDataModels>
}