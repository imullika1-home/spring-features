package com.example.springfeatures.service.user

import com.example.springfeatures.repository.user.UserRepository
import com.example.springfeatures.service.user.model.UserDataModels
import kotlinx.coroutines.reactive.awaitFirstOrNull
import kotlinx.coroutines.reactor.awaitSingle
import kotlinx.coroutines.reactor.awaitSingleOrNull
import org.elasticsearch.index.query.QueryBuilders.*
import org.springframework.data.domain.Pageable
import org.springframework.data.elasticsearch.core.ReactiveElasticsearchOperations
import org.springframework.data.elasticsearch.core.SearchHit
import org.springframework.data.elasticsearch.core.query.ByQueryResponse
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder
import org.springframework.data.elasticsearch.core.query.Query.findAll
import org.springframework.data.r2dbc.core.R2dbcEntityTemplate
import org.springframework.data.r2dbc.core.select
import org.springframework.data.relational.core.query.Criteria.where
import org.springframework.data.relational.core.query.Query.query
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.util.function.Tuple2

@Service
class UserServiceImpl(
    private val reactPostgre: R2dbcEntityTemplate,
    private val reactElastic: ReactiveElasticsearchOperations,
    private val reactElasticRepo: UserRepository
) : UserService {
    /**
     * Add or update user
     *
     * @param user [UserDataModels] user model that will be added to db if not exist else updated
     *
     * @return [reactor.util.function.Tuple2] of two [Mono]'s representing result of es & pg result
     */
    override suspend fun addOrUpdate(user: UserDataModels): Tuple2<UserDataModels, UserDataModels>? = Mono.zip(
        // Save to ES
        reactElastic.save(user),
        // Save to PG
        reactPostgre
            .exists(query(where("id").`is`(user.id)), UserDataModels::class.java)
            .flatMap {
                if (it) reactPostgre.update(user)
                else reactPostgre.insert(user)
            }
    ).awaitSingleOrNull()

    /**
     * Add or update user in elasticsearch database
     *
     * @param user [UserDataModels] user
     *
     * @return [UserDataModels]
     */
    override suspend fun addOrUpdateEs(user: UserDataModels): UserDataModels = reactElastic.save(user).awaitSingle()

    /**
     * Add or update user in elasticsearch database using repository
     *
     * @param user [UserDataModels] user
     *
     * @return [UserDataModels]
     */
    override suspend fun addOrUpdateEsRepo(user: UserDataModels): UserDataModels =
        reactElasticRepo.save(user).awaitSingle()

    /**
     * Add or update users in elasticsearch database
     */
    override fun addOrUpdateEsBulk(
        users: Iterable<UserDataModels>
    ): Flux<UserDataModels> = reactElastic.saveAll(users, UserDataModels::class.java)

    /**
     * Add or update users in elasticsearch database using repository
     */
    override fun addOrUpdateEsRepoBulk(
        users: Iterable<UserDataModels>
    ): Flux<UserDataModels> = reactElasticRepo.saveAll(users)

    /**
     * Find all users from postgresql data-base
     *
     * @return [reactor.core.publisher.Flux] of [UserDataModels]
     */
    override fun findAllPostgreSQL(): Flux<UserDataModels> = reactPostgre
        .select<UserDataModels>().all()

    /**
     * Find all users from elasticsearch database
     *
     * @return [reactor.core.publisher.Flux] of [UserDataModels]
     */
    override fun findAllElasticsearch(): Flux<SearchHit<UserDataModels>> = reactElastic
        .search(findAll(), UserDataModels::class.java)

    /**
     * Find all users from elasticsearch database using repository
     *
     * @return [reactor.core.publisher.Flux] of [UserDataModels]
     */
    override fun findAllElasticsearchRepo(): Flux<UserDataModels> = reactElasticRepo.findAll()

    /**
     * Find user by id in elasticsearch database
     *
     * @param id [String] user's id
     *
     * @return [UserDataModels] user
     */
    override suspend fun findByIdElasticsearch(id: String): UserDataModels? = reactElastic.search(
        NativeSearchQueryBuilder().withQuery(matchQuery("id", id)).build(),
        UserDataModels::class.java
    ).awaitFirstOrNull()?.content

    /**
     * Find user by id in elasticsearch database using repository
     *
     * @param id [String] user's id
     *
     * @return [UserDataModels] user
     */
    override suspend fun findByIdElasticsearchRepo(id: String) = reactElasticRepo.findById(id).awaitFirstOrNull()

    /**
     * Find user by id in postgresql database
     *
     * @param id [String] user's id
     *
     * @return [UserDataModels] user
     */
    override suspend fun findByIdPostgreSQL(id: String): UserDataModels? = reactPostgre.selectOne(
        query(where("id").`is`(id)),
        UserDataModels::class.java
    ).awaitSingleOrNull()

    /**
     * Delete user by id from es & pg data-bases
     *
     * @param id [String] user's id
     *
     * @return [reactor.util.function.Tuple2] of two [Mono]'s representing result of es & pg result
     */
    override suspend fun deleteById(id: String): Tuple2<String, Int>? = Mono.zip(
        reactElastic.delete(id, UserDataModels::class.java),
        reactPostgre.delete(query(where("id").`is`(id)), UserDataModels::class.java)
    ).awaitSingleOrNull()

    /**
     * Delete user by id from es database
     *
     * @param id [String] user's id
     *
     * @return [String] user's id
     */
    override suspend fun deleteByIdEs(id: String): String? =
        reactElastic.delete(id, UserDataModels::class.java).awaitSingleOrNull()

    /**
     * Delete user by id from es database using repository
     *
     * @param id [String] user's id
     *
     * @return [Void]
     */
    override suspend fun deleteByIdEsRepo(id: String): Void? = reactElasticRepo
        .existsById(id)
        .flatMap { if (it) reactElasticRepo.deleteById(id) else Mono.justOrEmpty(null) }.awaitSingleOrNull()

    /**
     * Bulk delete users by ids at elasticsearch database
     * @param ids [Iterable] of [String] user's ids
     */
    override suspend fun deleteByIdEsBulk(ids: Iterable<String>): ByQueryResponse = Flux
        .fromIterable(ids)
        .collectList()
        .map { reactElastic.idsQuery(it) }
        .flatMap { reactElastic.delete(it, UserDataModels::class.java) }
        .awaitSingle()

    override suspend fun deleteByIdElasticsearchRepoBulk(ids: Iterable<String>) = reactElasticRepo.deleteAllById(ids).awaitSingleOrNull()

    /**
     * Find users that name like in args and limit result count
     *
     * @param name [String] user's name that will be like
     * @param limit [Int] limit of result count
     *
     * @return [reactor.core.publisher.Flux] of [UserDataModels]
     */
    override fun findNameLikePostgreSQL(name: String, limit: Int): Flux<UserDataModels> = reactPostgre
        .select(UserDataModels::class.java)
        .matching(query(where("name").like("%$name%").ignoreCase(true)).limit(limit))
        .all()

    /**
     * Find users that name like in args and limit result count
     *
     * @param name [String] user's name that will be like
     * @param limit [Int] limit of result count
     *
     * @return [reactor.core.publisher.Flux] of [UserDataModels]
     */
    override fun findNameLikeElasticsearch(name: String, limit: Int): Flux<UserDataModels> = reactElastic.search(
        NativeSearchQueryBuilder()
            .withQuery(
                boolQuery().must(
                    queryStringQuery("$name*")
                        .field("name")
                        .analyzeWildcard(true)
                )
            )
            .withPageable(Pageable.ofSize(limit))
            .build(),
        UserDataModels::class.java
    ).map { it.content }

    override fun findNameLikeElasticsearchRepository(name: String, limit: Int): Flux<UserDataModels> = reactElasticRepo
        .findByNameLike(name, Pageable.ofSize(limit))
}