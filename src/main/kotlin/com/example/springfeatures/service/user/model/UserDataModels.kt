package com.example.springfeatures.service.user.model

import com.example.springfeatures.controller.user.model.UserDTO
import org.springframework.data.annotation.Id
import org.springframework.data.elasticsearch.annotations.Document
import org.springframework.data.elasticsearch.annotations.Field
import reactor.core.publisher.Mono

/**
 * Data model representing user
 * [@Document], [@Id] & [@Field] annotations for Elasticsearch
 * [@Table], [@Id] & [@Column] annotations for PostgreSQL
 * @param id [String] user's id
 * @param name [String] user's name
 */
//@Table("users")
@Document(indexName = "users")
data class UserDataModels(
    @Id
//    @Column("id")
    @Field(name = "id")
    val id: String,
//    @Column("name")
    @Field(name = "name")
    var name: String
) {
    /**
     * Convert data model to data transfer object
     * @return [Mono] of [UserDTO]
     */
    fun toDTO(): Mono<UserDTO> = Mono.just(UserDTO(id, name))
}