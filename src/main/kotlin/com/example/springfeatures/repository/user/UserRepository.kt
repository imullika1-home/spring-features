package com.example.springfeatures.repository.user

import com.example.springfeatures.service.user.model.UserDataModels
import org.springframework.data.domain.Pageable
import org.springframework.data.elasticsearch.annotations.Query
import org.springframework.data.repository.query.Param
import org.springframework.data.repository.reactive.ReactiveCrudRepository
import reactor.core.publisher.Flux

interface UserRepository : ReactiveCrudRepository<UserDataModels, String> {
    fun findByNameLike(name: String, page: Pageable): Flux<UserDataModels>
}