package com.example.springfeatures.repository.custom

import com.example.springfeatures.service.custom.model.CustomDataModels
import org.springframework.data.repository.reactive.ReactiveCrudRepository

interface CustomRepository : ReactiveCrudRepository<CustomDataModels, String> {
//    @Query("{\"query\":{\"bool\":{\"should\":[{\"ids\":{\"values\":[\":ids\"],\"boost\":1.0}},{\"terms\":{\"address.street\":[\":streets\"],\"boost\":1.0}},{\"terms\":{\"devices\":[\":devices\"],\"boost\":1.0}}],\"adjust_pure_negative\":true,\"minimum_should_match\":\":threshold\",\"boost\":1.0}}}")
//    fun theFilter(
//        @Param("ids") ids: Iterable<String>,
//        @Param("streets") streets: Iterable<String>,
//        @Param("devices") devices: Iterable<String>,
//        @Param("threshold") threshold: String): Flux<CustomDataModels>

//    fun findAllByIdInOrAddress_StreetInOrDevicesIn(
//        id: Flux<String>, address_street: Flux<String>, devices: Flux<String>
//    ): Flux<CustomDataModels>
}