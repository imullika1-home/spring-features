package com.example.springfeatures

import com.example.springfeatures.controller.user.UserController
import com.example.springfeatures.controller.user.model.UserDTO
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.test.web.reactive.server.WebTestClient
import org.springframework.test.web.reactive.server.body
import org.springframework.test.web.reactive.server.expectBodyList
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ApplicationTests {
    private val usersVM = mutableListOf(
        UserDTO("1", "name_1"),
        UserDTO("2", "name_2"),
        UserDTO("3", "name_3"),
    )

    @MockBean
    private lateinit var userController: UserController

    @Autowired
    private lateinit var webClient: WebTestClient

    @Test
    @DisplayName("Getting all users from PostgreSQL")
    fun findAllPg(): Unit {
        Mockito.`when`(userController.findAllPg()).thenReturn(Flux.fromIterable(usersVM))

        webClient
            .get()
            .uri("/users/pg")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isOk
            .expectBodyList(UserDTO::class.java)
            .contains(*usersVM.toTypedArray())
            .hasSize(3)
            .consumeWith<WebTestClient.ListBodySpec<UserDTO>>(System.out::println)
    }

    @Test
    @DisplayName("Getting all users from Elasticsearch")
    fun findAllEs(): Unit {
        Mockito.`when`(userController.findAllEs()).thenReturn(Flux.fromIterable(usersVM))

        webClient
            .get()
            .uri("/users/es")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isOk
            .expectBodyList(UserDTO::class.java)
            .contains(*usersVM.toTypedArray())
            .hasSize(3)
            .consumeWith<WebTestClient.ListBodySpec<UserDTO>>(System.out::println)
    }

    @Test
    @DisplayName("Find by id in PostgreSQL")
    fun findByIdPg(): Unit = runBlocking {
        val user = UserDTO("5", "name_5")
        Mockito.`when`(userController.findByIdPg(user.id)).thenReturn(user)

        val result = webClient
            .get()
            .uri("users/pg/${user.id}")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isOk
            .expectBody(UserDTO::class.java)
            .returnResult()

        assert(result.responseBody!! == user)
    }

    @Test
    @DisplayName("Add user to pg & es db")
    fun addUser(): Unit = runBlocking {
        val user = UserDTO("6", "name_6")
        Mockito.`when`(userController.add(user)).thenReturn(ResponseEntity<Any>(HttpStatus.OK))

        webClient
            .post()
            .uri("/users/add-or-update")
            .body(Mono.just(user))
            .exchange()
            .expectStatus().isOk
    }

    @Test
    @DisplayName("Delete user by id in pg & es db")
    fun deleteById(): Unit = runBlocking {
        val user = UserDTO("7", "name_7")
        Mockito.`when`(userController.deleteById(user.id)).thenReturn(ResponseEntity(HttpStatus.OK))

        webClient
            .get()
            .uri("/users/del/${user.id}}")
            .exchange()
            .expectStatus().isOk
    }

    @Test
    @DisplayName("Delete user by id that not exist")
    fun deleteByIdNotExist(): Unit = runBlocking {
        Mockito.`when`(userController.deleteById(anyString())).thenReturn(ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR))
        webClient
            .get()
            .uri("/users/del/fake-id")
            .exchange()
            .expectStatus().is5xxServerError
    }

    @Test
    @DisplayName("Find users that name like")
    fun findNameLikePg(): Unit {
        val limit = 3L
        val usersLike = Flux.fromIterable(
            listOf(
                UserDTO("1", "name_1"),
                UserDTO("2", "name_2"),
                UserDTO("3", "name_3")
            )
        ).take(limit)

        Mockito
            .`when`(userController.findNameLikePg("name", limit.toInt()))
            .thenReturn(usersLike)

        webClient
            .get()
            .uri {
                it
                    .path("/users/pg/like")
                    .queryParam("name", "name")
                    .queryParam("limit", limit)
                    .build()
            }
            .exchange()
            .expectStatus().isOk
            .expectBodyList<UserDTO>()
            .hasSize(limit.toInt())
            .consumeWith<WebTestClient.ListBodySpec<UserDTO>>(::println)
    }
}
