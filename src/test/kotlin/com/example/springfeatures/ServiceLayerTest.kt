package com.example.springfeatures

import com.example.springfeatures.controller.user.model.UserDTO
import com.example.springfeatures.service.user.UserService
import com.example.springfeatures.service.user.UserServiceImpl
import com.example.springfeatures.service.user.model.UserDataModels
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.web.reactive.server.WebTestClient
import org.springframework.test.web.reactive.server.WebTestClient.ListBodySpec
import org.springframework.test.web.reactive.server.body
import org.springframework.test.web.reactive.server.expectBody
import org.springframework.test.web.reactive.server.expectBodyList
import reactor.core.publisher.Flux
import reactor.kotlin.core.publisher.toFlux
import reactor.test.StepVerifier
import reactor.util.function.Tuples

/**
 * Service layer tests
 *
 * @property webClient [org.springframework.test.web.reactive.server.DefaultWebTestClient] that implement [WebTestClient] interface, using for making http requests
 * @property userService [UserServiceImpl] that implement [UserService] interface, using for [UserDataModels] manipulating
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ServiceLayerTest {
    @Autowired
    private lateinit var webClient: WebTestClient

    @MockBean
    private lateinit var userService: UserServiceImpl

    @Test
    @DisplayName("[PostgreSQL]: Find all")
    fun findAllPg() {
        // Flux of user data models
        val usersFlux = Flux.just(
            UserDataModels("1", "name_1"),
            UserDataModels("2", "name_2"),
            UserDataModels("3", "name_3")
        )

        // Mocking findAllPostgreSQL in /users/pg endpoint
        Mockito
            .`when`(userService.findAllPostgreSQL())
            .thenReturn(usersFlux)

        // Making request & save response for checking
        val response = webClient
            .get()
            .uri("/users/pg")
            .exchange()
            .expectStatus().isOk // HTTP 200
            .expectBodyList<UserDTO>() // Expecting user data transfer objects
            .hasSize(3) // Expecting 3 users
            .consumeWith<ListBodySpec<UserDTO>>(::println) // Print result
            .returnResult()

        // Verify response
        StepVerifier
            .create(response.responseBody!!.toFlux())
            .expectSubscription()
            .expectNextMatches { it.id == "1" }
            .expectNextMatches { it.id == "2" }
            .expectNextMatches { it.id == "3" }
            .verifyComplete()
    }

    @Test
    @DisplayName("[Elasticsearch]: Find by id")
    fun findByIdEs(): Unit = runBlocking {
        // User data model
        val user = UserDataModels("1", "name_1")

        // Mocking findByIdElasticsearch in /users/es/{id} endpoint
        Mockito.`when`(userService.findByIdElasticsearch(user.id)).thenReturn(user)

        // Making request & save response for checking
        val response = webClient
            .get()
            .uri("/users/es/${user.id}")
            .exchange()
            .expectStatus().isOk // HTTP 200
            .expectBody<UserDTO>() // Expecting user data transfer object
            .consumeWith(::println) // Print result
            .returnResult()

        // Verify response
        assert(response.responseBody?.id == user.id)
    }

    @Test
    @DisplayName("[All]: Add or update")
    fun addOrUpdate(): Unit = runBlocking {
        // User data model
        val user = UserDataModels("1", "name_1")

        // Mocking addOrUpdate in /users/add-or-update endpoint
        Mockito.`when`(userService.addOrUpdate(user)).thenReturn(Tuples.of(user, user))

        // Making request & check status
        webClient
            .post()
            .uri("/users/add-or-update")
            .body(user.toDTO())
            .exchange()
            .expectStatus().isOk // HTTP 200
    }

    @Test
    @DisplayName("[All]: Delete by id")
    fun deleteById(): Unit = runBlocking {
        // User data model
        val user = UserDataModels("1", "name_1")

        // Mocking deleteById in /users/del/{id} endpoint
        Mockito.`when`(userService.deleteById(user.id)).thenReturn(Tuples.of(user.id, 1))

        // Making request & check status
        webClient
            .get()
            .uri("/users/del/${user.id}")
            .exchange()
            .expectStatus().isOk // HTTP 200
    }

    @Test
    @DisplayName("[Elasticsearch]: Find name like")
    fun findNameLikeEs() {
        // Searching name
        val name = "name"
        // Limit of result records
        val limit = 3

        // Flux of user data models
        val usersFlux = Flux.just(
            UserDataModels("1", "name_1"),
            UserDataModels("2", "name_2"),
            UserDataModels("3", "name_3")
        ).take(limit.toLong())

        // Mocking findNameLikeElasticsearch in /users/es/like endpoint
        Mockito
            .`when`(userService.findNameLikeElasticsearch(name, limit))
            .thenReturn(usersFlux)

        // Making request & save response for checking
        val response = webClient
            .get()
            .uri{
                it
                    .path("/users/es/like")
                    .queryParam("name", name)
                    .queryParam("limit", limit)
                    .build()
            }
            .exchange()
            .expectStatus().isOk // HTTP 200
            .expectBodyList<UserDTO>() // Expecting user data transfer objects
            .hasSize(limit) // Expecting limited users
            .consumeWith<ListBodySpec<UserDTO>>(::println) // Print result
            .returnResult()

        // Verify response
        StepVerifier
            .create(response.responseBody!!.toFlux())
            .expectNextCount(limit.toLong())
            .thenConsumeWhile { it.name.lowercase().contains(name.lowercase()) }
            .verifyComplete()
    }
}